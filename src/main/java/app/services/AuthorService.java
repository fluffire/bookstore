package app.services;

import app.entities.Author;
import app.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    public Author getAuthorById(int id) {
        return authorRepository.findAuthorById(id);
    }

}
