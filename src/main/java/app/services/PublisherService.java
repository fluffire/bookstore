package app.services;

import app.entities.Publisher;
import app.repositories.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {
    @Autowired
    private PublisherRepository publisherRepository;

    public Publisher getPublisherById(int id) {
        return publisherRepository.getPublisherById(id);
    }
}
