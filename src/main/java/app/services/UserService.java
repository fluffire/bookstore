package app.services;

import app.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {
    public User addNewUser(String username, String password);
}
