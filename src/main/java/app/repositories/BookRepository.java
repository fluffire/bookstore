package app.repositories;

//import app.entities.Author;
//import app.entities.Publisher;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;
//import org.springframework.stereotype.Component;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
//import org.springframework.data.jpa.repository.JpaRepository;
import app.entities.Book;


@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {
    Book findBookById(int id);
}
