package app.repositories;

import app.entities.Publisher;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends PagingAndSortingRepository<Publisher, Integer> {
    Publisher getPublisherById(int id);
}
