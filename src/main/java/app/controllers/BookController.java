package app.controllers;

import app.entities.Book;
import app.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/books")
public class BookController {
    private final int PAGE_CAPACITY = 20;
    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }
    @GetMapping
    public String showBooksList(Model model, @RequestParam(name = "page", required = false) Integer pageNumber) {
        if (pageNumber == null)
            pageNumber = 1;
        model.addAttribute("books", bookService.getAllBooks(pageNumber - 1, PAGE_CAPACITY));
        return "books";
    }
    @GetMapping("/{id}")
    public String showBookPage(Model model, @PathVariable(value="id") int id) {
        Book book = bookService.getBookById(id);
        model.addAttribute("book", book);
        return "book";
    }
}
