package app.controllers;

import app.entities.User;
import app.services.UserService;
//import app.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/registration")
    public String showRegistrationPage(Model model) {
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", userService.loadUserByUsername("default"));
        }
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(Model model, String username, String password) {
        User newUser = userService.addNewUser(username, password);
        model.addAttribute("user", userService.loadUserByUsername(newUser.getName()));
        return "redirect:registration";
    }
}
