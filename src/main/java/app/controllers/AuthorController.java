package app.controllers;

import app.entities.Author;
import app.entities.Book;
import app.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/authors")
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/{id}")
    public String showAuthorPage(Model model, @PathVariable(value="id") int id) {
        Author author = authorService.getAuthorById(id);
        model.addAttribute("author", author);
        return "author";
    }

}
