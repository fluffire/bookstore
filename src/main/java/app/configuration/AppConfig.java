package app.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EnableJpaRepositories("app.repositories")
@ComponentScan(basePackages =
        "app.configuration;app.controllers;app.services")
@EntityScan("app.entities")
public class AppConfig {
}
