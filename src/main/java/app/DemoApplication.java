package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;


import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {
	private static ApplicationContext applicationContext;
	public static void main(String[] args) {
		applicationContext = SpringApplication.run(DemoApplication.class, args);

//		System.out.println(Arrays.toString(applicationContext.getBeanDefinitionNames()));
//		String[] beans = new String[]{"bookController", "mainController", "appConfig", "securityConfig", "securityWebAppInitializer"};
//		for (String beanName : beans) {
//			System.out.println("Is " + beanName + " in ApplicationContext: " +
//					applicationContext.containsBean(beanName));
//		}
	}

}
