package app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Cascade;

import java.util.List;

@Data
@Entity
@Table(name = "publishers")
public class Publisher {
    @Id
    private int id;
    @Column
    private String name;
    @JsonIgnore
    @OneToMany(mappedBy = "publisher")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<Book> books;
    @Override
    public String toString() {
        return id + " " + name;
    }
}
