package app.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table(name="books")
public class Book {
    @Id
    private int id;
    @Column
    private String title;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="book_authors",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private List<Author> authors;
    @ManyToOne
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;
    @Column
    private String genre;
    @JsonProperty("publishing_year")
    @Column(name = "publishing_year")
    private String publishingYear;
    @Column
    private BigDecimal price;
    public Book() {}
}
