package app.Deserializing;

import app.entities.Author;
import app.entities.Book;
import app.entities.Publisher;
import app.entities.Role;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    private static SessionFactory factory;
    private static Session session;
    private static List<Book> readFromJson(String filename) {
        ObjectMapper om = new ObjectMapper();
        File input = new File(filename);
        List<Book> books;
        try {
            books = Arrays.asList(om.readValue(input, Book[].class));
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        books.forEach(o -> System.out.println(o + "\n------------"));
        return books;
    }
    private static void mergeListWithDB(List<Book> books) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            for (Book book : books) {
                for (Author author : book.getAuthors())
                    session.merge(author);
                session.merge(book.getPublisher());
                session.persist(book);
            }
            session.getTransaction().commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            factory.close();
        }
        finally {
            session.close();
        }
    }
    public static void main(String[] args) {
        List<Book> books = readFromJson("C:\\Users\\Ivan\\Desktop\\программы\\python\\labirint4.json");
        assert books != null;
        factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Book.class)
                .addAnnotatedClass(Author.class)
                .addAnnotatedClass(Publisher.class)
                .buildSessionFactory();
        session = null;

        mergeListWithDB(books);

        try  {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Book b1 = session.get(Book.class, 1010815);
            System.out.println(b1.getAuthors());
            session.getTransaction().commit();
        }
        finally {
            session.close();
            factory.close();
        }
        List<Role> roles = new ArrayList<>();
        Role r1 = new Role();
        r1.setId(1L);
        r1.setName("USER");
        Role r2 = new Role();
        r2.setId(2L);
        r2.setName("ADMIN");
        roles.add(r1);
        roles.add(r2);
        System.out.println(roles.stream().peek(System.out::println)
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList()));
    }
}
